/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.disscrollview.sample;

import com.example.disscrollview.ResourceTable;
import com.flavienlaurent.discrollview.lib.Discrollvable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.app.Context;

/**
 * DiscrollvablePurpleLayout
 *
 * @since 2021-03-01
 */
public class DiscrollvablePurpleLayout extends DirectionalLayout implements Discrollvable {
    private static final int ONEFIVEZERO = 150;
    private static final float ZEROFIVE = 0.5f;
    private Image mPurpleView1;
    private Image mPurpleView2;

    private float mPurpleView1TranslationX = -ONEFIVEZERO;
    private float mPurpleView2TranslationX = ONEFIVEZERO;

    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollvablePurpleLayout(Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollvablePurpleLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollvablePurpleLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 初始化
     */
    protected void init() {
        if (mPurpleView1 == null || mPurpleView2 == null) {
            mPurpleView1 = (Image) findComponentById(ResourceTable.Id_purpleView1);
            mPurpleView2 = (Image) findComponentById(ResourceTable.Id_purpleView2);
        }
    }

    @Override
    public void onDiscrollve(float ratio) {
        init();
        if (ratio <= ZEROFIVE) {
            mPurpleView2.setTranslationX(0);
            mPurpleView2.setAlpha(0.0f);
            float rratio = ratio / ZEROFIVE;
            mPurpleView1.setTranslationX(mPurpleView1TranslationX * (1 - rratio));
            mPurpleView1.setAlpha(rratio);
        } else {
            mPurpleView1.setTranslationX(0);
            mPurpleView1.setAlpha(1.0f);
            float rratio = (ratio - ZEROFIVE) / ZEROFIVE;
            mPurpleView2.setTranslationX(mPurpleView2TranslationX * (1 - rratio));
            mPurpleView2.setAlpha(rratio);
        }
    }

    @Override
    public void onResetDiscrollve() {
        init();
        mPurpleView1.setAlpha(0);
        mPurpleView2.setAlpha(0);
        mPurpleView1.setTranslationX(mPurpleView1TranslationX);
        mPurpleView2.setTranslationX(mPurpleView2TranslationX);
    }
}
