/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.disscrollview.sample;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import com.example.disscrollview.ResourceTable;
import com.flavienlaurent.discrollview.lib.AttrUtil;
import com.flavienlaurent.discrollview.lib.Discrollvable;


/**
 * DiscrollvableGreenLayout
 *
 * @since 2021-03-01
 */
public class DiscrollvableGreenLayout extends DirectionalLayout implements Discrollvable {
    private static final int ONEZEROZERO = 100;
    private static final float ZEROFIVE = 0.5f;
    private Text mGreenView1;
    private float mGreenView1TranslationY = -ONEZEROZERO;
    private int mGreenColor;
    private int mBlackColor;
    private AttrSet attrSet;

    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollvableGreenLayout(Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollvableGreenLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        this.attrSet = attrSet;
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollvableGreenLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 初始化
     */
    protected void init() {
        if (mGreenView1 == null) {
            mGreenView1 = (Text) findComponentById(ResourceTable.Id_greenView1);
            mGreenView1.setTextColor(Color.WHITE);
        }
    }

    @Override
    public void onDiscrollve(float ratio) {
        init();
        mGreenView1.setTranslationY(mGreenView1TranslationY * (1 - ratio));
        if (ratio >= ZEROFIVE) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(new RgbColor(
                (Integer) AttrUtil.evaluate(ratio, Color.GREEN.getValue(), Color.BLACK.getValue())).asArgbInt()));
            setBackground(shapeElement);
        }
        if (ratio == 1.0f) {
            mGreenView1.setTextColor(new Color(Color.getIntColor("#FFD7F167")));
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(0x00, 0x00, 0x00));
            setBackground(shapeElement);
        }
    }

    @Override
    public void onResetDiscrollve() {
        init();
        mGreenView1.setTranslationY(mGreenView1TranslationY);
        mGreenView1.setTextColor(Color.GREEN); // 设置背景颜色
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(0x00, 0x00, 0x00));
        setBackground(shapeElement);
    }
}
