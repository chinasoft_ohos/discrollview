/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.disscrollview.sample;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathEffect;
import ohos.agp.render.PathMeasure;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.example.disscrollview.ResourceTable;
import com.flavienlaurent.discrollview.lib.Discrollvable;

/**
 * DiscrollvablePathLayout
 *
 * @since 2021-03-01
 */
public class DiscrollvablePathLayout extends DirectionalLayout implements Discrollvable {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "DiscrollvablePathLayout");
    private static final int TWOZERO = 20;
    private static final int TWO = 2;
    private static final float FIVEZERO = 5.0f;
    private static final float ZEROFIVE = 0.5f;
    private float mRatio;
    private Paint mPaint;
    private Text mPathView;
    private PathMeasure mPathMeasure;
    private Path mPath = new Path();

    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollvablePathLayout(Context context) {
        super(context);
        initPaint();
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollvablePathLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initPaint();
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollvablePathLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initPaint();
    }

    private Component.DrawTask getDrawTask() {
        Component.DrawTask drawPic = (component, canvas) -> {
            if (mPathMeasure == null) {
                mPathMeasure = new PathMeasure(mPath, false);
            }
            makeAndMeasurePath();
            float length = mPathMeasure.getLength();
            PathEffect effect = new PathEffect(new float[]{length, length}, length * (1 - mRatio));
            mPaint.setPathEffect(effect);
            canvas.drawPath(mPath, mPaint);
        };
        return drawPic;
    }

    private void makeAndMeasurePath() {
        mPath.reset();
        if (mPathView == null) {
            mPathView = (Text) findComponentById(ResourceTable.Id_pathView);
        }
        float translationY = mPathView.getTranslationY();
        mPath.moveTo(mPathView.getLeft(), mPathView.getTop() + translationY);
        mPath.lineTo(mPathView.getLeft() + mPathView.getWidth()
            + AttrHelper.vp2px(TWOZERO, getContext()), mPathView.getTop() + translationY);
        mPath.lineTo(mPathView.getLeft() + mPathView.getWidth()
            + AttrHelper.vp2px(TWOZERO, getContext()), mPathView.getTop() + mPathView.getHeight()
            + translationY + AttrHelper.vp2px(TWOZERO, getContext()));
        mPath.lineTo(mPathView.getLeft(), mPathView.getTop() + mPathView.getHeight() + translationY
            + AttrHelper.vp2px(TWOZERO, getContext())); // 框的左边线与底边线
        mPath.close();
        mPathMeasure.setPath(mPath, false);
    }

    private void initPaint() {
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeWidth(FIVEZERO);
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.WHITE);
        addDrawTask(getDrawTask());
    }

    @Override
    public void onDiscrollve(float ratio) {
        mRatio = ratio;
        if (mPathView == null) {
            mPathView = (Text) findComponentById(ResourceTable.Id_pathView);
        }
        mPathView.setAlpha(ratio);
        mPathView.setTranslationY(-(mPathView.getHeight() / TWO) * ((ratio - ZEROFIVE) / ZEROFIVE));
        invalidate();
    }

    @Override
    public void onResetDiscrollve() {
        mRatio = 0.0f;
        if (mPathView == null) {
            mPathView = (Text) findComponentById(ResourceTable.Id_pathView);
        }
        mPathView.setAlpha(0);
        mPathView.setTranslationY(mPathView.getHeight() / TWO);
        invalidate();
    }
}
