/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.disscrollview.sample;

import com.example.disscrollview.ResourceTable;
import com.flavienlaurent.discrollview.lib.Discrollvable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * DiscrollvableRedLayout
 *
 * @since 2021-03-01
 */
public class DiscrollvableRedLayout extends StackLayout implements Discrollvable {
    private static final float ZEROSIXFIVE = 0.65f;
    private static final float ZEROTHREEFIVE = 0.35f;
    private static final int ONE = 1;
    private static final float ONEFIVE = 1.5f;
    private static final float ONETWO = 1.2f;
    private Component mRedView1;
    private Component mRedView2;
    private float mRedView1TranslationY;

    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollvableRedLayout(Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollvableRedLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollvableRedLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 初始化
     */
    protected void init() {
        if (mRedView1 == null || mRedView2 == null) {
            mRedView1 = findComponentById(ResourceTable.Id_redView1);
            mRedView1TranslationY = mRedView1.getTranslationY();
            mRedView2 = findComponentById(ResourceTable.Id_redView2);
        }
    }

    @Override
    public void onDiscrollve(float ratio) {
        init();
        if (ratio <= ZEROSIXFIVE) {
            mRedView1.setTranslationY(-ONE * (mRedView1.getHeight() / ONEFIVE) * (ratio / ZEROSIXFIVE));
        } else {
            float rratio = (ratio - ZEROSIXFIVE) / ZEROTHREEFIVE;
            rratio = Math.min(rratio, 1.0f);
            mRedView1.setTranslationY(-ONE * (mRedView1.getHeight() / ONEFIVE));
            mRedView2.setAlpha(1 * rratio);
            mRedView2.setScaleX(ONETWO * rratio);
            mRedView2.setScaleY(ONETWO * rratio);
        }
    }

    @Override
    public void onResetDiscrollve() {
    }
}
