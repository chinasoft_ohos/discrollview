/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.disscrollview.sample;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;

import com.example.disscrollview.ResourceTable;
import com.flavienlaurent.discrollview.lib.Discrollvable;

/**
 * DiscrollvableLastLayout
 *
 * @since 2021-03-01
 */
public class DiscrollvableLastLayout extends DependentLayout implements Discrollvable {
    private static final int FOURZEROZERO = 400;
    private static final int SIXZEROZERO = 600;
    private static final int THREEZEROZERO = 300;
    private static final int EIGHTZEROZERO = 800;
    private Component mLastView1;
    private Component mLastView2;
    private Component mLastView3;

    private float mLastView1TranslationX = -FOURZEROZERO;
    private float mLastView2TranslationX = FOURZEROZERO;
    private float mLastView3TranslationX = SIXZEROZERO;

    private float mLastView1TranslationY = -THREEZEROZERO;
    private float mLastView2TranslationY = FOURZEROZERO;
    private float mLastView3TranslationY = EIGHTZEROZERO;

    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollvableLastLayout(Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollvableLastLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollvableLastLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * 初始化
     */
    protected void init() {
        if (mLastView1 == null) {
            mLastView1 = findComponentById(ResourceTable.Id_lastView1);
        }
        if (mLastView2 == null) {
            mLastView2 = findComponentById(ResourceTable.Id_lastView2);
        }
        if (mLastView3 == null) {
            mLastView3 = findComponentById(ResourceTable.Id_lastView3);
        }
    }

    @Override
    public void onDiscrollve(float ratio) {
        init();
        mLastView1.setTranslation((int) (mLastView1TranslationX * (1 - ratio)),
            (int) mLastView1TranslationY * (1 - ratio));
        mLastView1.setAlpha(ratio);

        mLastView2.setTranslationX(mLastView2TranslationX * (1 - ratio));
        mLastView2.setTranslationY(mLastView2TranslationY * (1 - ratio));
        mLastView2.setAlpha(ratio);

        mLastView3.setTranslationX(mLastView3TranslationX * (1 - ratio));
        mLastView3.setTranslationY(mLastView3TranslationY * (1 - ratio));
        mLastView3.setAlpha(ratio);
    }

    @Override
    public void onResetDiscrollve() {
        init();
        mLastView1.setAlpha(0);
        mLastView1.setTranslationX(mLastView1TranslationX);
        mLastView1.setTranslationY(mLastView1TranslationY);
        mLastView2.setAlpha(0);
        mLastView2.setTranslationX(mLastView2TranslationX);
        mLastView2.setTranslationY(mLastView2TranslationY);
        mLastView3.setAlpha(0);
        mLastView3.setTranslationX(mLastView3TranslationX);
        mLastView3.setTranslationY(mLastView3TranslationY);
    }
}
