
# Discrollview

#### 项目介绍

- 项目名称：Discrollview
- 所属系列：openharmony第三方组件适配移植
- 功能：一个自定义scrollView控件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 0.0.2

#### 效果演示
![example](https://gitee.com/chinasoft_ohos/discrollview/tree/master/img/discollview_down.gif)
![example](https://gitee.com/chinasoft_ohos/discrollview/tree/master/img/discrollview_up.gif)
#### 安装教程

在moudle级别下的build.gradle文件中添加依赖，在dependencies标签中增加对libs目录下jar包的引用
 ```gradle
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/release/'
    }
}

// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:DiscrollView:1.0.0'
}

 ```

在sdk5，DevEco Studio2.1 beta3下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

       <com.flavienlaurent.discrollview.lib.DiscrollView
               ohos:id="$+id:scrollview"
               ohos:height="match_parent"
               ohos:width="match_parent"
               ohos:background_element="#FFFFFF"
               ohos:bottom_padding="16vp"
               ohos:layout_alignment="horizontal_center"
               xmlns:discrollve="http://schemas.huawei.com/res/res-auto"
              >
               <com.flavienlaurent.discrollview.lib.DiscrollViewContent
                   ohos:id="$+id:disScallContent"
                   ohos:visibility="visible"
                   ohos:height="match_content"
                   ohos:width="match_parent">
                   <Text
                       ohos:id="$+id:text1"
                       ohos:width="match_parent"
                       ohos:height="600vp"
                       ohos:background_element="#ffffffff"
                       ohos:layout_alignment="center"
                       ohos:text_color="#000000"
                       ohos:padding="25vp"
                       ohos:text_size="72fp"
                       ohos:multiple_lines="true"
                       ohos:text_alignment="center"
                       ohos:text_font="serif"
                       ohos:text="Do you love cheese?" />

                   <com.flavienlaurent.disscrollview.sample.DiscrollvablePurpleLayout
                       ohos:id="$+id:purpleLayout"
                       ohos:width="match_parent"
                       ohos:height="match_content"
                       ohos:background_element="$color:holo_violet_light"
                       ohos:orientation="vertical"
                       ohos:visibility="visible">
                       <Image
                           ohos:id="$+id:purpleView1"
                           ohos:alpha="0"
                           ohos:width="200vp"
                           ohos:height="120vp"
                           ohos:top_margin="25vp"
                           ohos:image_src="$media:cheese1"
                           ohos:translation_x="-150vp" />

                       <Image
                           ohos:id="$+id:purpleView2"
                           ohos:alpha="0"
                           ohos:width="220vp"
                           ohos:height="110vp"
                           ohos:layout_alignment="right"
                           ohos:bottom_margin="25vp"
                           ohos:top_margin="15vp"
                           ohos:image_src="$media:cheese2"
                           ohos:translation_x="100vp" />

                   </com.flavienlaurent.disscrollview.sample.DiscrollvablePurpleLayout>
                   <com.flavienlaurent.disscrollview.sample.DiscrollvablePathLayout
                       ohos:width="match_parent"
                       ohos:height="match_content"
                       ohos:background_element="#FF52A0F5"
                       ohos:padding="10vp">

                       <Text
                           ohos:id="$+id:pathView"
                           ohos:width="match_parent"
                           ohos:height="match_parent"
                           ohos:text_color="#000000"
                           ohos:top_margin="90vp"
                           ohos:left_margin="20vp"
                           ohos:right_margin="20vp"
                           ohos:multiple_lines="true"
                           ohos:text_font="serif"
                           ohos:layout_alignment="center"
                           ohos:text_alignment="center"
                           ohos:text="When the cheese comes out everybody's happy pecorino red leicester"
                           ohos:text_size="20fp" />

                   </com.flavienlaurent.disscrollview.sample.DiscrollvablePathLayout>
                   <com.flavienlaurent.disscrollview.sample.DiscrollvableRedLayout
                       ohos:width="match_parent"
                       ohos:height="400vp"
                       ohos:background_element="#FFFF0000"
                       ohos:visibility="visible">

                       <Text
                           ohos:id="$+id:redView1"
                           ohos:width="match_content"
                           ohos:height="match_content"
                           ohos:layout_alignment="center"
                           ohos:text_font="serif"
                           ohos:text_alignment="center"
                           ohos:padding="20vp"
                           ohos:text="When the cheese comes out everybody's happy pecorino red leicester. The big cheese cheese on toast ricotta cheesy grin smelly cheese manchego chalk and cheese macaroni cheese."
                           ohos:text_color="#ffffff"
                           ohos:multiple_lines="true"
                           ohos:text_size="18vp" />
                      <!-- ohos:text="center"-->
                       <Image
                           ohos:id="$+id:redView2"
                           ohos:width="200vp"
                           ohos:height="200vp"
                           ohos:layout_alignment="center"
                           ohos:top_margin="40vp"
                           ohos:right_margin="20vp"
                           ohos:alpha="0.0"
                           ohos:image_src="$media:cheese3"
                           ohos:scale_x="0.1"
                           ohos:scale_y="0.1"
                           ohos:translation_y="50vp" />
                   </com.flavienlaurent.disscrollview.sample.DiscrollvableRedLayout>
                   <com.flavienlaurent.disscrollview.sample.DiscrollvableGreenLayout
                       ohos:width="match_parent"
                       ohos:height="match_content"
                       ohos:visibility="visible"
                       ohos:background_element="$color:holo_green_light">

                       <Text
                           ohos:id="$+id:greenView1"
                           ohos:width="match_parent"
                           ohos:height="match_parent"
                           ohos:margin="50vp"
                           ohos:text_font="serif"
                           ohos:translation_y="-50vp"
                           ohos:text_alignment="center"
                           ohos:text_weight="700"
                           ohos:text="Finally ..."
                           ohos:text_size="36fp" />
                   </com.flavienlaurent.disscrollview.sample.DiscrollvableGreenLayout>
                   <com.flavienlaurent.disscrollview.sample.DiscrollvableLastLayout
                       ohos:width="match_parent"
                       ohos:height="300vp"
                       ohos:alignment="center">

                       <Image
                           ohos:id="$+id:lastView1"
                           ohos:translation_x="-400vp"
                           ohos:translation_y="-400vp"
                           ohos:alpha="0"
                           ohos:width="match_content"
                           ohos:height="match_content"
                           ohos:scale_x="1.0"
                           ohos:scale_y="1.0"
                           ohos:image_src="$media:ilovecheese_i" />

                       <Image
                           ohos:id="$+id:lastView2"
                           ohos:translation_x="400vp"
                           ohos:translation_y="400vp"
                           ohos:scale_x="1.0"
                           ohos:scale_y="1.0"
                           ohos:alpha="0"
                           ohos:width="match_content"
                           ohos:height="match_content"
                           ohos:align_right="$id:lastView3"
                           ohos:image_src="$media:ilovecheese_heart" />

                       <Image
                           ohos:id="$+id:lastView3"
                           ohos:translation_y="400vp"
                           ohos:alpha="0"
                           ohos:scale_x="1.0"
                           ohos:scale_y="1.0"
                           ohos:width="match_content"
                           ohos:height="match_content"
                           ohos:text_alignment="center"
                           ohos:below="$id:lastView1"
                           ohos:image_src="$media:ilovecheese_cheese" />
                   </com.flavienlaurent.disscrollview.sample.DiscrollvableLastLayout>

               </com.flavienlaurent.discrollview.lib.DiscrollViewContent>
           </com.flavienlaurent.discrollview.lib.DiscrollView>

        
        <com.flavienlaurent.disscrollview.discroll.DiscrollView
            ohos:id="$+id:scrollview"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:background_element="#FFFFFF"
            ohos:bottom_padding="16vp"
            ohos:layout_alignment="horizontal_center"
            xmlns:discrollve="http://schemas.huawei.com/res/res-auto"
        >
            ...包裹滑动部分
        </com.flavienlaurent.disscrollview.discroll.DiscrollView>


#### 测试信息

- CodeCheck代码测试无异常
- CloudTest代码测试无异常
- 病毒安全检测通过
- 当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息
    Copyright 2013 Flavien Laurent

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

