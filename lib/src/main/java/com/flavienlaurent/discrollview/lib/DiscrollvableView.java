/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.discrollview.lib;

import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * DiscrollvableView
 *
 * @since 2021-03-01
 */
public class DiscrollvableView extends StackLayout implements Discrollvable {
    private static final int TRANSLATION_FROM_TOP = 0x01;
    private static final int TRANSLATION_FROM_BOTTOM = 0x02;
    private static final int TRANSLATION_FROM_LEFT = 0x04;
    private static final int TRANSLATION_FROM_RIGHT = 0x08;
    private static final int ONE = -1;
    private float mDiscrollveThreshold;
    private int mDiscrollveFromBgColor;
    private int mDiscrollveToBgColor;
    private boolean isDiscrollveAlpha;
    private int mDiscrollveTranslation;
    private boolean isDiscrollveScaleX;
    private boolean isDiscrollveScaleY;
    private int mWidth;
    private int mHeight;

    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollvableView(Context context) {
        super(context);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollvableView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollvableView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * setDiscrollveTranslation
     *
     * @param discrollveTranslation
     * @throws IllegalArgumentException
     */
    public void setDiscrollveTranslation(int discrollveTranslation) {
        mDiscrollveTranslation = discrollveTranslation;
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_BOTTOM) && isDiscrollveTranslationFrom(TRANSLATION_FROM_TOP)) {
            throw new IllegalArgumentException("cannot translate from bottom and top");
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_LEFT) && isDiscrollveTranslationFrom(TRANSLATION_FROM_RIGHT)) {
            throw new IllegalArgumentException("cannot translate from left and right");
        }
    }

    /**
     * setDiscrollveThreshold
     *
     * @param discrollveThreshold
     *@throws IllegalArgumentException
     */
    public void setDiscrollveThreshold(float discrollveThreshold) {
        if (discrollveThreshold < 0.0f || discrollveThreshold > 1.0f) {
            throw new IllegalArgumentException("threshold must be >= 0.0f and <= 1.0f");
        }
        mDiscrollveThreshold = discrollveThreshold;
    }

    public void setDiscrollveFromBgColor(int discrollveFromBgColor) {
        mDiscrollveFromBgColor = discrollveFromBgColor;
    }

    public void setDiscrollveToBgColor(int discrollveToBgColor) {
        mDiscrollveToBgColor = discrollveToBgColor;
    }

    public void setDiscrollveAlpha(boolean isDiscrollveAlpha1) {
        isDiscrollveAlpha = isDiscrollveAlpha1;
    }

    public void setDiscrollveScaleX(boolean isDiscrollveScaleX1) {
        isDiscrollveScaleX = isDiscrollveScaleX1;
    }

    public void setDiscrollveScaleY(boolean isDiscrollveScaleY1) {
        isDiscrollveScaleY = isDiscrollveScaleY1;
    }

    @Override
    public void onDiscrollve(float ratio) {
        if (isDiscrollveAlpha) {
            setAlpha(0.0f);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_BOTTOM)) {
            setTranslationY(mHeight);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_TOP)) {
            setTranslationY(-mHeight);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_LEFT)) {
            setTranslationX(-mWidth);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_RIGHT)) {
            setTranslationX(mWidth);
        }
        if (isDiscrollveScaleX) {
            setScaleX(0.0f);
        }
        if (isDiscrollveScaleY) {
            setScaleY(0.0f);
        }
    }

    @Override
    public void onResetDiscrollve() {
        if (isDiscrollveAlpha) {
            setAlpha(0.0f);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_BOTTOM)) {
            setTranslationY(mHeight);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_TOP)) {
            setTranslationY(-mHeight);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_LEFT)) {
            setTranslationX(-mWidth);
        }
        if (isDiscrollveTranslationFrom(TRANSLATION_FROM_RIGHT)) {
            setTranslationX(mWidth);
        }
        if (isDiscrollveScaleX) {
            setScaleX(0.0f);
        }
        if (isDiscrollveScaleY) {
            setScaleY(0.0f);
        }
    }

    private boolean isDiscrollveTranslationFrom(int translationMask) {
        if (mDiscrollveTranslation == ONE) {
            return false;
        }
        return (mDiscrollveTranslation & translationMask) == translationMask;
    }
}
