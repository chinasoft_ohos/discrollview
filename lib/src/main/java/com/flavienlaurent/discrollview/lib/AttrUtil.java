/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.discrollview.lib;

import ohos.agp.components.AttrSet;

/**
 * AttrUtil
 *
 * @since 2021-03-01
 */
public class AttrUtil {
    private static final int TWOFOUR = 24;
    private static final int ONESIX = 16;
    private static final int EIGHT = 8;
    private static final int WHITE = 0xff;
    private static final float TWOFIVEFIVE = 255.0f;
    private static final float TWOTWO = 2.2f;
    AttrSet attrSet;

    /**
     * AttrUtil 构造函数
     *
     * @param attrSet
     */
    public AttrUtil(AttrSet attrSet) {
        this.attrSet = attrSet;
    }

    /**
     * getStringValue获取attr数据
     *
     * @param key
     * @param defValue
     * @return defValue
     */
    public String getStringValue(String key, String defValue) {
        if (attrSet.getAttr(key).isPresent()) {
            return attrSet.getAttr(key).get().getStringValue();
        } else {
            return defValue;
        }
    }

    /**
     * evaluate 颜色估值器
     *
     * @param fraction
     * @param startValue
     * @param endValue
     * @return Object
     */
    public static Object evaluate(float fraction, Object startValue, Object endValue) {
        int startInt = (Integer) startValue;
        float startA = ((startInt >> TWOFOUR) & WHITE) / TWOFIVEFIVE;
        float startR = ((startInt >> ONESIX) & WHITE) / TWOFIVEFIVE;
        float startG = ((startInt >> EIGHT) & WHITE) / TWOFIVEFIVE;
        float startB = (startInt & WHITE) / TWOFIVEFIVE;
        int endInt = (Integer) endValue;
        float endA = ((endInt >> TWOFOUR) & WHITE) / TWOFIVEFIVE;
        float endR = ((endInt >> ONESIX) & WHITE) / TWOFIVEFIVE;
        float endG = ((endInt >> EIGHT) & WHITE) / TWOFIVEFIVE;
        float endB = (endInt & WHITE) / TWOFIVEFIVE;
        startR = (float) Math.pow(startR, TWOTWO); // convert from sRGB to linear
        startG = (float) Math.pow(startG, TWOTWO);
        startB = (float) Math.pow(startB, TWOTWO);
        endR = (float) Math.pow(endR, TWOTWO);
        endG = (float) Math.pow(endG, TWOTWO);
        endB = (float) Math.pow(endB, TWOTWO);
        float a1 = startA + fraction * (endA - startA); // compute the interpolated color in linear space
        float r1 = startR + fraction * (endR - startR);
        float g1 = startG + fraction * (endG - startG);
        float b1 = startB + fraction * (endB - startB);
        a1 = a1 * TWOFIVEFIVE; // convert back to sRGB in the [0..255] range
        r1 = (float) Math.pow(r1, 1.0 / TWOTWO) * TWOFIVEFIVE;
        g1 = (float) Math.pow(g1, 1.0 / TWOTWO) * TWOFIVEFIVE;
        b1 = (float) Math.pow(b1, 1.0 / TWOTWO) * TWOFIVEFIVE;
        return Math.round(a1) << TWOFOUR | Math.round(r1) << ONESIX | Math.round(g1) << EIGHT | Math.round(b1);
    }
}

