/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.discrollview.lib;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**
 * DisScrollViewContent
 *
 * @since 2021-03-01
 */
public class DiscrollViewContent extends DirectionalLayout {
    private static final int ONE = -1;
    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollViewContent(Context context) {
        super(context);
        setOrientation(VERTICAL);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollViewContent(Context context, AttrSet attrSet) {
        super(context, attrSet);
        setOrientation(VERTICAL);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollViewContent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setOrientation(VERTICAL);
    }

    @Override
    public void addComponent(Component childComponent, int index, ComponentContainer.LayoutConfig layoutConfig) {
        super.addComponent(asDiscrollvable(childComponent,(LayoutConfig)layoutConfig), index, layoutConfig);
    }

    /**
     * asDiscrollvable
     *
     * @param child
     * @param lp
     * @return Component
     */
    private Component asDiscrollvable(Component child, LayoutConfig lp) {
        if (isDiscrollvable(lp)) {
            return child;
        }
        DiscrollvableView discrollvableChild = new DiscrollvableView(getContext());
        discrollvableChild.setDiscrollveAlpha(lp.isDiscrollveAlpha);
        discrollvableChild.setDiscrollveTranslation(lp.mDiscrollveTranslation);
        discrollvableChild.setDiscrollveScaleX(lp.mDiscrollveScaleX);
        discrollvableChild.setDiscrollveScaleY(lp.mDiscrollveScaleY);
        discrollvableChild.setDiscrollveThreshold(lp.mDiscrollveThreshold);
        discrollvableChild.setDiscrollveFromBgColor(lp.mDiscrollveFromBgColor);
        discrollvableChild.setDiscrollveToBgColor(lp.mDiscrollveToBgColor);
        discrollvableChild.addComponent(child);
        return discrollvableChild;
    }

    private boolean isDiscrollvable(LayoutConfig lp) {
        boolean isFlag = lp.isDiscrollveAlpha || lp.mDiscrollveTranslation != ONE || lp.mDiscrollveScaleX;
        return isFlag
            || lp.mDiscrollveScaleY
            || (lp.mDiscrollveFromBgColor != ONE && lp.mDiscrollveToBgColor != ONE);
    }

    /**
     * LayoutConfig
     *
     * @since 2021-03-01
     */
    public static class LayoutConfig extends DirectionalLayout.LayoutConfig {
        private int mDiscrollveFromBgColor;
        private int mDiscrollveToBgColor;
        private float mDiscrollveThreshold;
        private int mDiscrollveTranslation;
        private boolean isDiscrollveAlpha;
        private boolean mDiscrollveScaleX;
        private boolean mDiscrollveScaleY;

        /**
         * 构造函数
         *
         * @param context
         * @param attrSet
         */
        public LayoutConfig(Context context, AttrSet attrSet) {
            super(context, attrSet);
            isDiscrollveAlpha = AttrValue.get(attrSet,"discrollve_alpha",false);
            mDiscrollveScaleX = AttrValue.get(attrSet,"discrollve_scaleX",false);
            mDiscrollveScaleY = AttrValue.get(attrSet,"discrollve_scaleY",false);
            mDiscrollveTranslation = AttrValue.get(attrSet,"discrollve_translation",ONE);
            mDiscrollveThreshold = AttrValue.get(attrSet,"discrollve_fromBgColor",0.0f);
            mDiscrollveFromBgColor = AttrValue.get(attrSet,"discrollve_threshold",ONE);
            mDiscrollveToBgColor = AttrValue.get(attrSet,"discrollve_toBgColor",ONE);
        }

        /**
         * LayoutConfig
         *
         * @param w
         * @param h
         */
        public LayoutConfig(int w, int h) {
            super(w, h);
        }
    }
}
