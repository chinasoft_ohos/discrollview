/*
 * Copyright 2013 Flavien Laurent
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.flavienlaurent.discrollview.lib;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;


/**
 * DiscrollView
 *
 * @since 2021-03-01
 */
public class DiscrollView extends ScrollView {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "DiscrollView"); // 定义日志标签
    private static final int TWO = 2;
    private static final int ONEZEROZERO = 100;
    private DiscrollViewContent mContent;

    /**
     * 构造函数
     *
     * @param context
     */
    public DiscrollView(Context context) {
        super(context);
        this.setScrollbarRoundRect(true);
        this.setScrollbarFadingEnabled(true);
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     */
    public DiscrollView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        this.setScrollbarRoundRect(true);
        this.setScrollbarFadingEnabled(true);
        HiLog.info(LABEL_LOG, "zhangliang=>DiscrollView2");
    }

    /**
     * 构造函数
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public DiscrollView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.setScrollbarRoundRect(true);
        this.setScrollbarFadingEnabled(true);
    }

    /**
     * onFinishInflate
     *
     * @throws IllegalStateException
     */
    public void onFinishInflate() {
        if (getChildCount() != 1) {
            throw new IllegalStateException("Discrollview must host one child.");
        }
        Component content = getComponentAt(0);
        if (!(content instanceof DiscrollViewContent)) {
            throw new IllegalStateException("Discrollview must host a DiscrollViewContent.");
        }
        mContent = (DiscrollViewContent) content;
        if (mContent.getChildCount() < TWO) {
            throw new IllegalStateException("Discrollview must have at least 2 children.");
        }
        this.setScrolledListener(new ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                setupFirstView();
                onScrollChanged(i1);
            }
        });
    }

    private void setupFirstView() {
        Component first = mContent.getComponentAt(0); // first first take all the parent height
        if (first != null) {
            first.getLayoutConfig().height = getHeight();
        }
    }

    /**
     * clamp
     *
     * @param value
     * @param max
     * @param min
     * @return clamp
     */
    public static float clamp(float value, float max, float min) {
        return Math.max(Math.min(value, min), max);
    }

    private int getAbsoluteBottom() {
        Component last = getComponentAt(getChildCount() - 1);
        if (last == null) {
            return 0;
        }
        return last.getBottom();
    }

    private void onScrollChanged(int top) {
        int scrollViewHeight = getHeight();
        int scrollViewBottom = getAbsoluteBottom();
        int scrollViewHalfHeight = scrollViewHeight / TWO;
        for (int index = 0; index < mContent.getChildCount(); index++) {
            Component child = mContent.getComponentAt(index);
            if (!(child instanceof Discrollvable)) {
                continue;
            }
            Discrollvable discrollvable = (Discrollvable) child;
            int discrollvableTop = child.getTop();
            int discrollvableHeight = child.getHeight();
            int discrollvableAbsoluteTop = discrollvableTop - top;
            if (scrollViewBottom - child.getBottom()
                < discrollvableHeight + scrollViewHalfHeight
                - AttrHelper.vp2px(ONEZEROZERO, getContext())) { // 卸料器太大，到达中心时无法卸料器
                if (discrollvableAbsoluteTop <= scrollViewHeight) { // 可拆卸的顶部到达DiscrollView底部
                    int visibleGap = scrollViewHeight - discrollvableAbsoluteTop;
                    discrollvable.onDiscrollve(clamp(visibleGap / (float) discrollvableHeight, 0.0f, 1.0f));
                } else {
                    discrollvable.onResetDiscrollve();
                }
            } else {
                if (discrollvableAbsoluteTop <= scrollViewHalfHeight) {
                    int visibleGap = scrollViewHalfHeight
                        - discrollvableAbsoluteTop; // the Discrollvable center reaches the DiscrollView center
                    discrollvable.onDiscrollve(clamp(visibleGap / (float) discrollvableHeight, 0.0f, 1.0f));
                } else {
                    discrollvable.onResetDiscrollve();
                }
            }
        }
    }
}
